# CustomCreatures
A mod that makes it possible to add in new custom creatures easily without any coding.

You can add in new creatures by just creating a .creature file. Start by reading the .config
file fo the mod.

## Deity aggro

Sometimes mobs may ignore players if they are a certain deity. This may be undesirable, but
upon a quick look it seems unavoidable. If you experience this (and only then) you should use
Valhrek's fix for this: [Valhrek/CustomCreatureDeityPassiveAggro](https://github.com/Valhrek/CustomCreatureDeityPassiveAggro) 

## The properties
To read more about each property you can give to a new creature, consult the [Properties.md](./include/Properties.md)