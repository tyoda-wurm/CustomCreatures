
# Sections:
#  1. Introduction
#  2. How to add in a new creature
#  3. Actual config stuff



## Introduction

# Hi! Welcome to Custom Creatures! You can use this mod to add in new creatures
# without writing any code, just by adding in a new file and defining some properties
# for the creature.

# You can download the other zip file from gitlab named 'CustomCreatures-VERSION-creatures.zip' and
# extract it like any other mod to add in a bunch of custom creatures made by Tyrfang. Some of these
# will spawn on their own, some can only be summoned. You should inspect each of them to see which
# ones you want.



## How to add in a new creature

# You will have to create .creature files in the creatures directory here in the mod's directory.
# These .creature files behave similarly to .config files. Check out creatures/ExampleCreature1.creature
# for an example of how to use them.

# You can find Properties.md in the CustomCreatures folder for details on each
# property you may assign to a creature.
# You can also find other files with interesting values in there.
# To see what the values for any vanilla creature are, consult CreatureTemplates.md

# The only required property for a custom creature is the 'name'.
# By default the value of a property can be a variety of stuff. Change them if you don't like em.

# Changing the name of a creature that already exists might cause existing ones to
# not appear anymore. Look out for that.

# Be sure to not misspell the property names. The server will crash if it finds an unknown property name.

# If you rename a .creature file, for example 'Something.creature' to 'SomethingElse.creature', then
# the creatures that spawned in your world when the file had the first name will most likely disappear
# until you change it back.

# You may disable a creature one of three ways:
#  - deleting the .creature file
#  - defining 'disabled = true' in the .creature file
#  - if 'dotCreatureOnly' is set to true and you rename it to something not ending in .creature



## Actual config stuff

# Whether to only read from files that end with '.creature'.
# This is useful if you want to have other files in the creatures directory,
# or if you want to be able to disable creatures by for example renaming
# creature files to 'SOMETHING.creature.disabled'
# Default: true
dotCreatureOnly = true

# Whether to log the adding of new properties for each creature. This
# generates lots of spam on server startup and is not very useful usually.
# Default: false
logAddedCreatureProperties = false