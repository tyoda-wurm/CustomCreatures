
# This file is meant to help you get the IDs of the ***body types*** you wish to reference.

 - BodyHuman   : 0
 - BodyHorse   : 1
 - BodyBear    : 2
 - BodyDog     : 3
 - BodyEttin   : 4
 - BodyCyclops : 5
 - BodyDragon  : 6
 - BodyBird    : 7
 - BodySpider  : 8
 - BodySnake   : 9